一、集群所有机器执行准备操作
1、设置集群IP及域名映射，IP改成实际的，域名不动。
vi /etc/hosts
192.168.1.21 ka.1
192.168.1.22 ka.2
192.168.1.23 ka.3
保存
或执行命令
echo "192.168.1.21 ka.1" >> /etc/hosts
echo "192.168.1.22 ka.2" >> /etc/hosts
echo "192.168.1.23 ka.3" >> /etc/hosts


2、关闭防火墙或放开必要的端口 2888 3888 2181
systemctl disable firewalld
systemctl stop firewalld

二、在ka.1机器上执行安装命令
1、执行安装，中途会要求输入机器集群机器的root密码
type wget >/dev/null 2>&1 || yum install -y wget
wget http://down.coynn.top/kafka-andy/install.sh
sh install.sh

三、运维

#启动集群所有
/data/kafka/kafka_2.12-1.0.1/myshells/start-all-cluster.sh
#停止集群所有
/data/kafka/kafka_2.12-1.0.1/myshells/stop-all-cluster.sh

任何机器修改 zookeeper.properties 或server.properties文件后，执行一条命令同步到集群其他机器。
/data/kafka/kafka_2.12-1.0.1/myshells/properties.toall.sh

查看kafka服务端日志
tail -100f /data/kafka/kafka_2.12-1.0.1/logs/kafkaServer.out
查看zk日志
tail -100f /data/kafka/kafka_2.12-1.0.1/logs/zookeeper.out

四、测验

/data/kafka/kafka_2.12-1.0.1/bin/kafka-topics.sh --create --zookeeper ka.1:2181, ka.2:2181, ka.3:2181 --replication-factor 3 --partitions 3 --topic sum

1、发送消息
/data/kafka/kafka_2.12-1.0.1/bin/kafka-console-producer.sh --broker-list ka.1:9092, ka.2:9092, ka.3:9092 --topic summber
2、消费消息
/data/kafka/kafka_2.12-1.0.1/bin/kafka-console-consumer.sh --zookeeper ka.1:2181, ka.2:2181, ka.3:2181 --from-beginning --topic summber
