kafkahome=$(dirname $0)/../..
kafkahome=$(cd $kafkahome; pwd)
serverId=$1
sed -i "s#broker.id=.*#broker.id=$serverId#g" $kafkahome/config/server.properties
sed -i "s#host.name=.*#host.name=ka.$serverId#g" $kafkahome/config/server.properties
echo "$serverId" > /data/kafka/data.dir/zk/data/myid
