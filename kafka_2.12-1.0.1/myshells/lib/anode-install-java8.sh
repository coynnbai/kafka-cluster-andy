#!/bin/sh
kafkahome=$(dirname $0)/../..
kafkahome=$(cd $kafkahome; pwd)
function insjava8()
{
	cd /data/java
	tar -zxf jdk-8u161-linux-x64.tar.gz
	sed -i '2c JAVA_HOME=/data/java/jdk1.8.0_161;PATH=$PATH:$JAVA_HOME/bin;export JAVA_HOME PATH' /etc/profile
	source /etc/profile
}
type java >/dev/null 2>&1 || insjava8