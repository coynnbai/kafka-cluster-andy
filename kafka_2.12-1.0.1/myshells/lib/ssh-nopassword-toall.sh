#执行本机免密码登陆到集群机器,包括自己
#仅仅只会执行一次，如果需要再次执行，需要删除 conf.d/nopassword 文件。
kafkahome=$(dirname $0)/../..
kafkahome=$(cd $kafkahome; pwd)

serverIds=$(cat $kafkahome/myshells/conf.d/serverIds)
ls ~/.ssh/id_rsa.pub >/dev/null 2>&1 || ssh-keygen -t rsa
ls $kafkahome/myshells/conf.d/nopassword >/dev/null 2>&1 || foreach $serverIds ssh-copy-id root@ka.%id
mkdir -p $kafkahome/myshells/conf.d
echo 'nopassword done' > $kafkahome/myshells/conf.d/nopassword