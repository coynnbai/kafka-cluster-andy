#!/bin/sh

set -e
cd ~

kafkato=/data/kafka
java8to=/data/java

#安装必要工具命令
type wget >/dev/null 2>&1 || yum install -y wget
wget http://down.coynn.top/install-gget.sh
sh install-gget.sh
wget http://down.coynn.top/install-foreach.sh
sh install-foreach.sh


#下载kafka到 /tmp目录
rm -rf /tmp/kafkains
mkdir -p /tmp/kafkains/$kafkato
cd /tmp/kafkains/$kafkato

ls /tmp/kafka_2.12-1.0.1.tgz || (wget http://down.coynn.top/kafka_2.12-1.0.1.tgz;mv kafka_2.12-1.0.1.tgz /tmp/kafka_2.12-1.0.1.tgz)
\cp /tmp/kafka_2.12-1.0.1.tgz .
tar zxf kafka_2.12-1.0.1.tgz
rm -f kafka_2.12-1.0.1.tgz
#下载coynn's集群工具集合入kafka目录
gget https://gitlab.com/coynnbai/kafka-cluster-andy/repository/master/archive.tar
rm -f readme.txt

#下载java8到 /tmp目录
mkdir -p /tmp/kafkains/$java8to
cd /tmp/kafkains/$java8to
ls /tmp/jdk-8u161-linux-x64.tar.gz || (wget http://down.coynn.top/jdk-8u161-linux-x64.tar.gz;mv jdk-8u161-linux-x64.tar.gz /tmp/jdk-8u161-linux-x64.tar.gz)
\cp /tmp/jdk-8u161-linux-x64.tar.gz .


kafkahome=/tmp/kafkains/$kafkato/kafka_2.12-1.0.1

chmod +x $kafkahome/myshells/*.sh
chmod +x $kafkahome/myshells/lib/*.sh

serverIds=$(cat $kafkahome/myshells/conf.d/serverIds)

#集群节点免密码处理
sh $kafkahome/myshells/lib/ssh-nopassword-toall.sh
#所有复制kafka完整目录到所有集群节点
foreach $serverIds scp -r /tmp/kafkains/* root@ka.%id:/

kafkahome=$kafkato/kafka_2.12-1.0.1


#集群每个节点初始化
foreach $serverIds ssh root@ka.%id "'$kafkahome/myshells/lib/anode-install-kafka.sh'"
#集群每个节点安装java8，如已有java则跳过
foreach $serverIds ssh root@ka.%id "'$kafkahome/myshells/lib/anode-install-java8.sh'"

#每个节点属性文件更新serverid
sh $kafkahome/myshells/properties.toall.sh
